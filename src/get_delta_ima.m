function delta_ima = get_delta_ima(Toe,theta)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Another regression test is to verify that:
    % phi=90, theta=-90
    % Origin of Siesmoid Ridge (S) Coordinate system is globally located
    %        at x = 1/2W, y=G_1^d+G_4^d
    
    x=1; y=2; z=3; scale=4;            % Stores index of each dimension
    %    (dimension=Index)
    tolerance=0.00000000000001;        % For floating point comparisons
    
    OO=[0; 0; 0; 1;];                  % Origin of each coordinate system
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % A_{G} -> A_{O}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    T_GO=[...
        1 0 0 0;
        0 cosd(-Toe('eta')) -sind(-Toe('eta')) 0;
        0 sind(-Toe('eta')) cosd(-Toe('eta')) Toe('H1p');
        0 0 0 1;
    ];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  A_{O} -> A_{\Phi}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    T_OPHI=[...
        1 0 0 0;
        0 cosd(Toe('psi')) -sind(Toe('psi')) (Toe('L1p')+Toe('L1m'));
        0 sind(Toe('psi')) cosd(Toe('psi')) 0;
        0 0 0 1;
    ];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  A_{\phi} -> A_{n}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    T_PHIN=[...
        1 0 0 0;
        0 cosd(-Toe('psi')) -sind(-Toe('psi')) (Toe('L1d')-Toe('L1m'));
        0 sind(-Toe('psi')) cosd(-Toe('psi')) 0;
        0 0 0 1;
    ];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %  A_{n} -> A_{s}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    T_NS=[...
        1 0 0 0;
        0 1 0 0;
        0 0 1 -.5*Toe('W');
        0 0 0 1;
    ];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % A_{\Phi} -> A_{\theta}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    T_PHITHETA=[...
        cosd(theta) 0 sind(theta) 0;
        0 1 0 0;
        -sind(theta) 0 cosd(theta) 0;
        0 0 0 1;
    ];
    
    GS_wtheta=T_GO*T_OPHI*T_PHITHETA*T_PHIN*T_NS*OO;
    
    xt=GS_wtheta(x);
    yt=GS_wtheta(y);
    delta_ima=atand((xt/yt));
end