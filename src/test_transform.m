function [] = test_transform()
% TEST_TRANSFORM  small suite of tests to verify that the transformations
%                 produce results in agreement with the calculated geometries
%                 of the metatarsal.
%   Toe = Toe anatomy and surgical inputs and geometries for testing
%         kinematic transformations
%
% N.B:  Note that sind/cosd/tand are the preffered tools for this analysis.
%       The floating point error of pi propagates in addition to the
%       intrinsic error of the trigonometric functions.
%
clc;
clear();
format long;

toe_width=1;
toe_length=5;
ima_offset=3;
eta=30;
phi=90;%((pi/6)*(180/pi));%90;

Toe=initialize_toe(toe_length,toe_width,ima_offset,eta,phi);

x=1; y=2; z=3; scale=4;            % Stores index of each dimension
%    (dimension=Index)
tolerance=0.00000000000001;        % For floating point comparisons

OO=[0; 0; 0; 1;];                  % Origin of each coordinate system

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A_{G} -> A_{O}

T_GO=[...
    1 0 0 0;
    0 cosd(-Toe('eta')) -sind(-Toe('eta')) 0;
    0 sind(-Toe('eta')) cosd(-Toe('eta')) Toe('H1p');
    0 0 0 1;
    ];

GO=T_GO*OO;

if GO(z) ~= Toe('H1p')
    display('Error in transformation, T_GO');
else
    display('T_GO successfully transforms');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  A_{O} -> A_{\Phi}

T_OPHIa=[...
    1 0 0 0;
    0 1 0 (Toe('L1p')+Toe('L1m'));
    0 0 1 0;
    0 0 0 1;
    ];

T_OPHIb=[...
    1 0 0 0;
    0 cosd(Toe('psi')) -sind(Toe('psi')) 0;
    0 sind(Toe('psi')) cosd(Toe('psi')) 0;
    0 0 0 1;
    ];

T_OPHI=T_OPHIa*T_OPHIb;

GPHI=T_GO*T_OPHI*OO;

if (abs(GPHI(y) - Toe('G1m')) > tolerance)  || (abs(GPHI(z) - Toe('H1m')) > tolerance )
    display('Error in transformation, T_GO x T_OPHI');
    % Display translated coordinate, its expected 'adjacent' component
    % (i.e. y component) and its 'opposite' component (i.e. z component).
    display(GPHI); display(Toe('G1m')); display(Toe('H1m'));
else
    display('T_GO x T_OPHI successfully transforms');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  A_{\phi} -> A_{n}

T_PHINa=[...
    1 0 0 0;
    0 cosd(-Toe('psi')) -sind(-Toe('psi')) 0;
    0 sind(-Toe('psi')) cosd(-Toe('psi')) 0;
    0 0 0 1;
    ];

T_PHINb=[...
    1 0 0 0;
    0 1 0 (Toe('L1d')-Toe('L1m'));
    0 0 1 0;
    0 0 0 1;
    ];

T_PHIN=T_PHINa*T_PHINb;

GN=T_GO*T_OPHI*T_PHIN*OO;

if (abs(GN(y) - (Toe('G1d')+Toe('G4d')) ) > tolerance) || ...
        (abs(GN(z) - Toe('H2d')) > tolerance)
    display('Error in transformation, T_GO x T_OPHI x T_PHIN');
    display(GN); display(Toe('G1d')+Toe('G4d')); display(Toe('H2d'));
else
    display('T_GO x T_OPHI x T_PHIN successfully transforms');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  A_{n} -> A_{s}

T_NS=[...
    1 0 0 0;
    0 1 0 0;
    0 0 1 -.5*Toe('W');
    0 0 0 1;
    ];

GS=T_GO*T_OPHI*T_PHIN*T_NS*OO;

if ( abs(GS(z)) > tolerance ) || (abs(GS(y) - Toe('G1d')) > tolerance)
    display('Error in transformation, T_GO x T_OPHI x T_PHIN x T_NS');
    display(GN); display(Toe('G1d'));

else
    display('T_GO x T_OPHI x T_PHIN x T_NS successfully transforms');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A_{\Phi} -> A_{\theta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Testing
%   At any phi, where theta=360 => Distal portion of toe should be where it 
%                                started.
%   At phi=90, where theta=180 => Height = H_3^d (ToeConfig('W'))

assert(Toe('phi')==90);

for theta_rot=180:180:360
    T_PHITHETA=[...
        cosd(theta_rot) 0 sind(theta_rot) 0;
        0 1 0 0;
        -sind(theta_rot) 0 cosd(theta_rot) 0;
        0 0 0 1;
        ];
    
    GS_wtheta=T_GO*T_OPHI*T_PHITHETA*T_PHIN*T_NS*OO;
    
    if theta_rot==180
        if ( abs(GS_wtheta(z) - Toe('H1d') ) > tolerance ) || ...
                (abs(GS_wtheta(y) - (Toe('G1d')+Toe('G5d')) ) > tolerance)
            display('Error in transformation, T_GO x T_OPHI x T_PHITHETA x T_PHIN x T_NS');
            display(GS_wtheta); display(Toe('H1d')); display(Toe('G1d'));
        else
            display('T_GO x T_OPHI x T_PHITHETA x T_PHIN x T_NS successfully transforms');
        end
    else
        if ( abs(GS_wtheta(z)) > tolerance  ) || ...
                (abs(GS_wtheta(y) - Toe('G1d') ) > tolerance)
            display('Error in transformation, T_GO x T_OPHI x T_PHITHETA x T_PHIN x T_NS');
            display(GS_wtheta); display(Toe('H1d')); display(Toe('G1d'));
        else
            display('T_GO x T_OPHI x T_PHITHETA x T_PHIN x T_NS successfully transforms');
        end
    end
end
