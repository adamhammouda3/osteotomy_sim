% main function for perforing the kinetic transformations necessary
% to display data about the simulated operations:
% *elevation                       z-dimension of from A_{G}-A_{s} origin
% *inter-metatarsal-angle (ima)/change ima  from x-dim of A_{G}-A_{s}
% *pronation

clc;
clear();
format long;
colors={'.b','.c','.r','.g'};

toe_width=1;
toe_length=5;
ima_offset=3;
eta=30;
%phi=90;

%Toe=initialize_toe(toe_length,toe_width,ima_offset,eta,phi);


% Basic plotting of clinical values

phis=[((pi/6)*(180/pi));
    ((pi/4)*(180/pi));
    ((pi/3)*(180/pi));
    ((pi/2)*(180/pi));];

thetas=[];
heights=[];
dimas=[];
pronations=[];
heights2D=[];
dimas2D=[];
pronations2D=[];

for i=1:length(phis)
    phi=phis(i);
    Toe=initialize_toe(toe_length,toe_width,ima_offset,eta,phi);
    for theta=1:1:360
        height=get_height(Toe,theta);
        heights=[heights height];
        
        delta_ima=get_delta_ima(Toe,theta);
        dimas=[dimas delta_ima];
        
        pronation=get_pronation(Toe,theta);
        pronations=[pronations pronation];
        
        if phi==((pi/6)*(180/pi))
            thetas=[thetas theta];
        end
    end
    heights2D=vertcat(heights2D,heights);
    dimas2D=vertcat(dimas2D,dimas);
    pronations2D=vertcat(pronations2D,pronations);
    heights=[]; dimas=[]; pronations=[];
end

% HEIGHT
figure;
hold on;
for i=1:length(phis)
    color=char(colors(i))
    plot(thetas,heights2D(i,:),color);
end

set(gca,'FontSize',20,'Fontweight','b');
xlim([1 360]);
xlabel('Degrees of Rotation');
ylabel('Height of Distal Bottom of Metatarsal');
legend('\Phi = \pi / 6','\Phi = \pi / 4',...
    '\Phi = \pi / 3','\Phi = \pi / 2');
hold off;

% \delta IMA
figure;
hold on;
for i=1:length(phis)
    color=char(colors(i))
    plot(thetas,dimas2D(i,:),color);
end

set(gca,'FontSize',20,'Fontweight','b');
xlim([1 360]);
xlabel('Degrees of Rotation');
ylabel('\Delta IMA');
legend('\Phi = \pi / 6','\Phi = \pi / 4',...
    '\Phi = \pi / 3','\Phi = \pi / 2');
hold off;

% PRONATION
figure;
hold on;
% Because of the overlap of each set of data, its necessary to have some additional
% distinguishing features for these linespecs
lwidth=[8.0; 6.0; 4.0; 2.0;];
colors={'-xg','-.m',':r','.k'};

for i=1:length(phis)
    color=char(colors(i));
    plot(thetas,pronations2D(i,:),color,'Linewidth',lwidth(i));
end

set(gca,'FontSize',20,'Fontweight','b');
xlim([1 360]);
xlabel('Degrees of Rotation');
ylabel('Pronation (Degrees)');
legend('\Phi = \pi / 6','\Phi = \pi / 4',...
    '\Phi = \pi / 3','\Phi = \pi / 2');
hold off;
