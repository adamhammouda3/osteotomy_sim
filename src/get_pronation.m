function pronation=get_pronation(Toe,theta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This will be the angle of A_S' x-axis with A_G's x-axis

x=1; y=2; z=3; scale=4;            % Stores index of each dimension
%    (dimension=Index)
tolerance=0.00000000000001;        % For floating point comparisons

OO=[0; 0; 0; 1;];                  % Origin of each coordinate system

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A_{G} -> A_{O}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_GO=[...
    1 0 0 0;
    0 cosd(-Toe('eta')) -sind(-Toe('eta')) 0;
    0 sind(-Toe('eta')) cosd(-Toe('eta')) Toe('H1p');
    0 0 0 1;
    ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  A_{O} -> A_{\Phi}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_OPHIa=[...
    1 0 0 0;
    0 1 0 (Toe('L1p')+Toe('L1m'));
    0 0 1 0;
    0 0 0 1;
    ];

T_OPHIb=[...
    1 0 0 0;
    0 cosd(Toe('psi')) -sind(Toe('psi')) 0;
    0 sind(Toe('psi')) cosd(Toe('psi')) 0;
    0 0 0 1;
    ];

T_OPHI=T_OPHIa*T_OPHIb;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  A_{\phi} -> A_{n}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_PHINa=[...
    1 0 0 0;
    0 cosd(-Toe('psi')) -sind(-Toe('psi')) 0;
    0 sind(-Toe('psi')) cosd(-Toe('psi')) 0;
    0 0 0 1;
    ];

T_PHINb=[...
    1 0 0 0;
    0 1 0 (Toe('L1d')-Toe('L1m'));
    0 0 1 0;
    0 0 0 1;
    ];

T_PHIN=T_PHINa*T_PHINb;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  A_{n} -> A_{s}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_NS=[...
    1 0 0 0;
    0 1 0 0;
    0 0 1 -.5*Toe('W');
    0 0 0 1;
    ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A_{\Phi} -> A_{\theta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_PHITHETA=[...
    cosd(theta) 0 sind(theta) 0;
    0 1 0 0;
    -sind(theta) 0 cosd(theta) 0;
    0 0 0 1;
    ];



O1=[1; 0; 0; 1;];

% Transform new x-axis segment of A_s into global coordinate space
GS0=T_GO*T_OPHI*T_PHITHETA*T_PHIN*T_NS*OO;
GS1=T_GO*T_OPHI*T_PHITHETA*T_PHIN*T_NS*O1;

a=[(GS1(x)-GS0(x)) (GS1(z)-GS0(z))];
b=[(O1(x)-OO(x)) (O1(z)-O1(z))];

% Calculate the angle between vectors
costheta = dot(a,b)/(norm(a)*norm(b));
pronation = acosd(costheta);

% t_ratio=abs(GS1(y)-GS0(y))/abs(GS1(x)-GS0(x));
% 
% if (GS1(x)-GS0(x))>0 && (GS1(y)-GS0(y))>0
%     pronation=atand(t_ratio);
% elseif (GS1(x)-GS0(x))<=0 && (GS1(y)-GS0(y))>0
%     pronation=180-atand(t_ratio);
% elseif (GS1(x)-GS0(x))<0 && (GS1(y)-GS0(y))<=0
%     pronation=180+atand(t_ratio);
% elseif (GS1(x)-GS0(x))>=0 && (GS1(y)-GS0(y))<=0
%     pronation=180+(180-atand(t_ratio));
% end
end