function Toe = initialize_toe(length, width, offset, eta, phi)
% INITIALIZE_TOE return a container.Map with the necessary geometries to 
% run tests on the kinematic system
%   length = Length of the metatarsal
%   width  = Height/Width of metatarsal (Height=Width)
%   offset = Distance b/w point of IMA measurement and proximal
%            end of metatarsal
%   eta    = Typicl angle of distal point of metatarsal with ground
%   phi    = Incision angle

clear Toe;
Toe=containers.Map;

% %%%%%%%%%  'Independent' Anatomical/Surgical terms  %%%%%%%%%%%%%%%%%%%%%
Toe('phi')=phi;    % incision angle
Toe('eta')=eta;    % Typicl angle of distal point of metatarsal with ground
Toe('L1d')=length; % Length of the metatarsal
Toe('L1p')=offset; % Distance b/w point of IMA measurement and proximal
                   % end of metatarsal
Toe('W')=width;    % Height/Width of metatarsal (Height=Width)

% %%%%%%%%%  'Dependent' Anatomical/Surgical Terms  %%%%%%%%%%%%%%%%%%%%%%
Toe('eta_bar')=90-Toe('eta'); % eta's compliment

Toe('psi')=90-Toe('phi');

Toe('L1m')=Toe('L1d')/2;
Toe('L2d')=Toe('L1d')-Toe('L1m');

Toe('G3d')=(.5*Toe('W'))/cosd( Toe('eta_bar') );
Toe('G4d')=(.5*Toe('W'))*cosd( Toe('eta_bar') );
Toe('G5d')=Toe('W')*cosd( Toe('eta_bar') );

Toe('L3d')=Toe('G3d')*sind( Toe('eta_bar') );
Toe('L4d')=Toe('L2d')+Toe('L3d');

Toe('G1d')=(Toe('L1d')+Toe('L1p')+Toe('L3d'))*cosd(Toe('eta')) - Toe('G3d');
Toe('G2d')=Toe('L4d')*cosd( Toe('eta') ) - Toe('G3d');
Toe('G1m')=Toe('G1d')-Toe('G2d');

Toe('H1p')=(Toe('L1d')+Toe('L1p')+Toe('L3d'))*sind( Toe('eta') );
Toe('H1m')=Toe('L4d')*sind( Toe('eta') );
Toe('H1d')=sind(Toe('eta_bar'));
Toe('H2d')=(.5*Toe('W'))*sind( Toe('eta_bar') );

% %%%%%%%%%  Transformation Matrices  %%%%%%%%%%%%%%%%%%%%%%