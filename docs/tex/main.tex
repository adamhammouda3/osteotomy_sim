\documentclass[12pt]{article}

%\usepackage{algpseudocode}
%\usepackage{tikz}
%\usetikzlibrary{arrows}

\usepackage{ulem}
\usepackage{listings}
\usepackage{amssymb,amsmath}
\usepackage{enumitem}
\usepackage{mathtools}

%\usepackage{precalc}
%\usepackage {mcode} 

%% Figures Path and packages
\usepackage{lscape}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{color}
\usepackage{epstopdf}
\graphicspath{{../figures/}}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linktoc=all,
    citecolor=black,
    filecolor=black,
    linkcolor=blue,
    urlcolor=blue
}

\begin{document}
\title{Hallux Valgus Bunion Kinematics} 
\author{Adam Hammouda and Vinay Sharma}
\date{\today}
\maketitle

\section{Methods:  Modeling the Metatarsal and Osteotomy}

In order to generate an arbitrary and extendable model of a metatarsal experiencing 
hallux valgus and the ludloff osteotomy, we employed the mathematical formalisms
of \href{http://elvis.rowan.edu/~kay/papers/kinematics.pdf}{robot kinematics}.  
A diagram of the kinematic system is given in 
Figure \ref{fig:big_diagram}.  A coordinate system, $A$ is subscripted with it's location
on the metatarsal and it's relationship to the dimensions relevant to the procedure.  For 
example, $A_G$ is the 'ground' coordinate frame.  

Naming conventions for the other variables are as follows.  $H_i^j$ is the height from the 
ground of element of the metatarsal, or 
the kinematic system overlaying it.  Here $j$ is $p$,$m$,$d$ indicating that the 
given height is located proximally, medially, or distally on the metarsal.  $i$ is
an arbitrary indexing term to differentiate between the height terms.  
The terms $G_i^j$, and $L_i^j$ correspond to the same conventions, but for edge lengths
parallel with the ground and the length of the metatarsal respectively.

\begin{figure}
\centering
\centering
\begin{subfigure}[b]{0.7\textwidth}
  \includegraphics[width=\textwidth]{model_diagram_big.pdf}
  \caption{Coordinate systems which form the basis of our kinematic transformations.}
  \label{fig:big_diagram}
\end{subfigure}

\begin{subfigure}[b]{0.7\textwidth}
  \includegraphics[width=\textwidth]{zoom_diagram.pdf}
  \caption{Dimensions of the distal portion of the metatarsal.}
  \label{fig:zoom_diagram}
\end{subfigure}

\caption{Coordinate Systems and Variables relevent for the kinematic transformations 
  employed in this study's analysis of hallux valgus.  For the closed form, or qualitative
  description of each variable and their relationships to each other see 
  Figure \ref{fig:diagram_terms}.}
\label{fig:diagrams}
\end{figure}

\begin{figure}
  \begin{align}
    \eta &= \text{The angle between the distal point of the metatarsal with 
      the ground}\\ 
    \overline{\eta} &= 90-\eta\\
    L_1^d  &= \text{The length of the metatarsal}\\
    L_1^p  &= \text{The distance between the point of the apex of IMA and the 
      beginning of the metatarsal.}\\
    \phi   &= \text{Angle of incision}\\
    \overline{\phi}   &= 90-\phi\\
    \theta &= \text{Angle of rotation}\\
    & \\
    L_1^m &= \frac{L_1^d}{2}\\
    L_2^d &= L_1^d-L_1^m\\
    G_3^d &= \frac{\frac{1}{2}W}{cos(\overline{\eta})}\\
    G_4^d &= \frac{1}{2}W cos(\overline{\eta})\\
    G_5^d &= W cos(\overline{\eta})\\
    & \\
    L_3^d &= G_3^dsin(\overline{\eta})\\
    L_4^d &= L_2^d+L_3^d\\
    & \\
    G_1^d &= (L_3^d+L_1^d+L_1^p)cos(\eta) - G_3^d\\
    G_2^d &= L+4^dcos(\eta)-G_3^d\\
    G_1^m &= G_1^d-G_2^d\\
    & \\
    H_1^p &= (L_3^d+L_1^d+L_1^p)sin(\eta)\\
    H_1^m &= L_4^dsin(\eta)\\
    H_1^d &= sin(\overline{\eta})\\
    H_2^d &= \frac{1}{2}W sin(\overline{\eta})
  \end{align}
  \caption{The closed form interpretation of the variables used in Figure \ref{fig:zoom_diagram} 
    and Figure \ref{fig:big_diagram}.}
  \label{fig:diagram_terms}
\end{figure}
  
The transformations between each coordinate 
frame given in Figure \ref{fig:big_diagram} are as follows:

\begin{itemize}
\item $T_G^O :=$ Transformation From our Ground/'global' coordinate 
  system to apex of our IMA (From $A_G \rightarrow A_O$).
\item $T_O^{\phi}:=$ Transformation from the apex of our IMA to the 
  plane of incision (From $A_O \rightarrow A_{\phi}$).
\item $ T_{\phi}^{\theta}:=$ Transformation within the plane of incision
  to any rotation of the toe about theta (From $A_{\phi} \rightarrow A_{\theta}$).
\item $ T_{\phi}^{n}:=$ Transformation within the plane of incision to a 
  point halfway along the anterior of the metatarsal. $n$ hear standing 
  for normal, as the transformation is normal to the anterior of the metatarsal.
\item $ T_{n}^{s}:=$ Transformation from the normal axis to the seismoid 
  ridge of the toe.
\end{itemize}

\begin{equation}
T_G^O = \begin{bmatrix}
  1 & 0             & 0     	   &   0      \\[0.3em]
  0 & cos(-\eta)  & -sin(-\eta) &   0      \\[0.3em]
  0 & sin(-\eta)  &  cos(-\eta) &   H_1^P      \\[0.3em]
  0 & 0            &  0            &    1  	        
\end{bmatrix}
\end{equation}
\newline
This of course assumes that the whole toe block is tilted up from the ground at 
$\eta$, which is what we've illustrated in Figure 1.\newline
\newline
To define the next matrix we'll define a term $\overline{\phi} \equiv 90-\phi$, which is 
the angle by which the x-axis must be rotated from $A_O$ to get to $A_{\phi}$.  
$L_1^p+L_1^m$ then is the distance translated, just like $H_1^P$ above
\begin{equation}
  T_O^{\phi} = \begin{bmatrix}
    1 & 0             & 0     	   &   0      \\[0.3em]
    0 & cos(\overline{\phi})  & -sin(\overline{\phi}) &   (L_1^p+L_1^m)      \\[0.3em]
    0 & sin(\overline{\phi})  &  cos(\overline{\phi}) &   0      \\[0.3em]
    0 & 0            &  0            &    1  	        
  \end{bmatrix}
\end{equation}
\begin{equation}
  T_{\phi}^{\theta} = \begin{bmatrix}
    cos(\theta) & 0    & sin(\theta)   &   0      \\[0.3em]
    0	         & 1    &  0  		       &   0      \\[0.3em]
    -sin(\theta)	& 0   &  cos(\theta)  &   0      \\[0.3em]
    0 		& 0  &  0          	       &   1  	        
  \end{bmatrix}
\end{equation}
Which is just the standard rotation matrix about the y axis.
\begin{equation}
  T_{\phi}^{n} = \begin{bmatrix}
    1 & 0           &  0          &   0      \\[0.3em]
    0 & cos(-\overline{\phi})  & -sin(-\overline{\phi}) & (L_1^d-L_1^m)\\[0.3em]
    0 & sin(-\overline{\phi})  &  cos(-\overline{\phi}) & 0      \\[0.3em]
    0 & 0           &  0          &    1
  \end{bmatrix}
\end{equation}

\begin{equation}
  T_n^{s} = \begin{bmatrix}
    1 & 0  & 0  &   0      \\[0.3em]
    0 & 1  & 0  &   0      \\[0.3em]
    0 & 0  &  1 &   -\frac{1}{2}W      \\[0.3em]
    0 & 0  &  0 &    1
  \end{bmatrix}
\end{equation}

\section{Testing the Model}

Most of the lengths outlined in the Figure \ref{fig:diagrams} and Figure \ref{fig:diagram_terms}
 are not necessary to track the procedure, or relevent anatomical features of the 
metatarsal, but they do provide trigonometric checks that our transformations have been successful.
Each non-essential term in Figure \ref{fig:diagram_terms} corresponds to an $x$,$y$ value with 
respect to the coordinate frame $A_G$ in Figure \ref{fig:big_diagram}.  Using these values, we can 
compare them to the $x$,$y$ values that are produced by homogeneously transforming the origin of 
each kinematic frame of reference (($x$,$y$,$z$)=($0$,$0$,$0$)) back to the origin using the 
transformation matrices listed above.  Because the trigenometric values agree with the kinematic 
transformations, we have confidence that they are being used correctly to deliver the clinically 
relevent terms.

\section{Deriving Clinically Relevent Terms From The Kinematic System}

Let a point, $p$, in an arbitary coordinate system in a kinematic system be defined by
$$
p=\left\[x,y,z,1\right\]
$$
where $x$, $y$, $z$ are the coordinates of the point, and $1$ is a scaling factor of 
distances between each point.  Let $\left\(\textsf{dim}\right\)$ be an operation which
 takes such a point and returns the value of the coordinate $\textsf{dim}$, where 
$\textsf{dim}=x,y,\text{ or } z$.

\subsection{Elevation}
\begin{figure}
  \includegraphics[scale=0.5]{elevation.pdf}
  \caption{Example of Elevation.  Note that it's measured perpendicular from the ground
    to the midpoint of the distal base of the metatarsal.}
  \label{fig:elevation}
\end{figure}

Figure~\ref{fig:elevation} illustrates an example of a post-operative elevation 
measurement.  Obtaining this value from the mathematical framework can be done as 
follows:
\[
\text{Elevation}\equiv (T_G^O)(T_O^{\phi})
\]

\subsection{$\delta$IMA}
\begin{figure}
  \includegraphics[scale=0.5]{dima.pdf}
  \caption{Example of $\delat$IMA.}
  \label{fig:dima}
\end{figure}

\subsection{Pronation}
\begin{figure}
  \includegraphics[scale=0.5]{pronation.pdf}
  \caption{Example of pronation.}
  \label{fig:pronation}
\end{figure}



\section{Basic Results}

The 3 clinically important terms we derive from the system presently are the 
distal elevation of the metatarsal after it's rotation (the 'height'), the 
change in the intermetatarsal angle due to rotation, and the pronation of the
toe due to rotation along the plane of incision.  All of these values are 
plotted with respect to the Degrees of rotation for 4 different angles of 
incision.  While most of these values are not clinically relevent, it 
illustrates that our model behaves effectively as an abstract mechanical 
system, where we need only impose a few constraints to derive clinically 
meaningful data.  Results are given in Figure~\ref{fig:height_results},
~\ref{fig:dima_results}, and~\ref{fig:pronation_results}.


\begin{figure}
  \includegraphics[width=\textwidth]{height_results}
  \caption{}
  \label{fig:height_results}
\end{figure}

\begin{figure}
  \includegraphics[width=\textwidth]{dima_results}
  \caption{}
  \label{fig:dima_results}
\end{figure}

\begin{figure}
  \includegraphics[width=\textwidth]{pronation_results}
  \caption{}
  \label{fig:pronation_results}
\end{figure}

\end{document}
