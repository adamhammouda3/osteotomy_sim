Repository For Osteotomy Research

Author's:
-------------
Adam Hammouda
Vinay Sharma

Last Updated
------------
January 26, 2014

TODO
----
* Remove the repetition of the transformation matrices by 
  coding up an object (classdef) which can get and set their parameterization
  according to the Toe container specs
* Move the Toe container initialization to a classdef:
  http://www.mathworks.com/company/newsletters/articles/introduction-to-object-oriented-programming-in-matlab.html
  http://www.mathworks.com/help/matlab/object-oriented-programming.html
* Build GUI for adjustment and display of surgical parameters and terms
